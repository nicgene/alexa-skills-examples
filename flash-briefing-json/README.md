# Flash Briefing JSON

Convert a Reddit JSON feed to compatible alexa skill format.

## Run service offline

```bash
docker-compose up
```

## Re-build the image
```bash
docker-compose up --build
```

## Access the JSON feeds
[http://0.0.0.0:3003/politics](http://0.0.0.0:3003/politics)
[http://0.0.0.0:3003/news](http://0.0.0.0:3003/news)
[http://0.0.0.0:3003/world-news](http://0.0.0.0:3003/world-news)
[http://0.0.0.0:3003/entertainment](http://0.0.0.0:3003/entertainment)

## Notes
You can access the shells for our images with the following (example) command:

```bash
docker exec -it flash-briefing-json_serverless_1 /bin/ash
```