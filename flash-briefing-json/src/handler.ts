import axios from 'axios';

const alexaFeed = async (url: string) => {
  let array = [];
  
  try {
    let response = await axios.get(url);
    let data = response.data.data.children;

    for (let i = 0; i < 5; i++) {
      array.push({
        uid: data[i].data.id,
        updateDate: new Date(data[i].data.created * 1000),
        titleText: data[i].data.title.normalize(),
        // mainText: data[i].data.selftext ? data[i].data.selftext.normalize() : '',
        mainText: data[i].data.title.normalize(),
        redirectionUrl: data[i].data.url
      })
    }

    return {
      statusCode: 200,
      body: JSON.stringify(array)
    }
  } catch (error) {
    console.error(error);
  }
}

export const entertainment = async () => await alexaFeed('https://www.reddit.com/r/entertainment/.json');

export const news = async () => await alexaFeed('https://www.reddit.com/r/news/.json');

export const politics = async () => await alexaFeed('https://www.reddit.com/r/politics/.json');

export const worldNews = async () => await alexaFeed('https://www.reddit.com/r/worldnews/.json');